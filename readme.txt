https://docs.aws.amazon.com/en_us/AmazonECR/latest/userguide/docker-basics.html

Task Definitions
- create iam role
    - Elastic Container Service
        - Case: Service Task
        - Find Ecs Task Excludtion role

- create load balancer
    - internet-facing ต่อภายนอก, internal ต่อภายใน
    - vpc default
    - Configure Security ssl
    - Target group: กลุ่มของเครื่อง , Health checks: เช็คว่าเครื่องไหนตาย แล้วจะสร้างเครื่องใหม่ ทำตัวเช็คกำหนด response status 200
- cluster
    - config Service
        - Maximum percent: 250
    - Deployments
        - Rolling update: deploy แล้วจบ rowback ไม่ได้
        - Blue/green deployment สามารถ rowback ได้
    - Service discovery เชื่อโยง service เข้าด้วยกัน